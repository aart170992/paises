﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace Paises.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public List<negocio.negociosPais.NegocioPais> Get()
        {
            negocio.negociosPais.AdministradorDePais metodosNegocioPais = new negocio.negociosPais.AdministradorDePais(WebConfigurationManager.AppSettings["cadenaDeconexionSQLSERVER"]);
            var listaPaises = metodosNegocioPais.obtenerTodosPaises();
            return listaPaises;
        }

        // GET api/values/5
        public negocio.negociosPais.NegocioPais Get(string nombrePais)
        {
            negocio.negociosPais.AdministradorDePais metodosNegocioPais = new negocio.negociosPais.AdministradorDePais(WebConfigurationManager.AppSettings["cadenaDeconexionSQLSERVER"]);
            negocio.negociosPais.NegocioPais datoPais = new negocio.negociosPais.NegocioPais();
            datoPais = metodosNegocioPais.obtenerPais(nombrePais);
            return datoPais;
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
            var cliente = new RestClient();
            var request = new RestRequest("https://restcountries.eu/rest/v2/all", Method.GET);
            var paises = cliente.Execute<List<negocio.negociosPais.NegocioPais>>(request).Data;


            negocio.negociosPais.AdministradorDePais metodosNegocioPais = new negocio.negociosPais.AdministradorDePais(WebConfigurationManager.AppSettings["cadenaDeconexionSQLSERVER"]);
            metodosNegocioPais.insertarPais(paises);
        }

        // PUT api/values/5
        public negocio.negociosPais.NegocioPais Put([FromBody]negocio.negociosPais.NegocioPais paisActualizar)
        {
            negocio.negociosPais.AdministradorDePais metodosNegocioPais = new negocio.negociosPais.AdministradorDePais(WebConfigurationManager.AppSettings["cadenaDeconexionSQLSERVER"]);
            var paisActualizado = metodosNegocioPais.actualizarPais(paisActualizar);
            return paisActualizado;
        }

        // DELETE api/values/5
        public void Delete(string nombrePais)
        {
            negocio.negociosPais.AdministradorDePais metodosNegocioPais = new negocio.negociosPais.AdministradorDePais(WebConfigurationManager.AppSettings["cadenaDeconexionSQLSERVER"]);
            metodosNegocioPais.eliminarPais(nombrePais);

        }
    }
}
