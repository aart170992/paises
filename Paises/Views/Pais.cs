﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Paises.Views
{
    public class Pais
    {
        public string name { get; set; }
        public string capital { get; set; }
        public string region { get; set; }
        public string subregion { get; set; }
        public string population { get; set; }
     

    }
}