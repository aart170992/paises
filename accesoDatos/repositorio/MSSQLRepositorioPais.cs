﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace accesoDatos.repositorio
{
    public class MSSQLRepositorioPais: core.MSSQLAdministradorConexion
    {
        public MSSQLRepositorioPais(string cadenaConexion) : base(cadenaConexion) {

        }

        public void insertar(accesoDatos.clases.Paisdb datosPais)
        {
           

            using (var connection = new SqlConnection(this.cadenaDeConexionInterna))
            {
                try
                {
                    connection.Open();
                    string script = "INSERT INTO pais(pais_nombre, pais_capital, pais_region, pais_subregion, pais_population) VALUES (@nombre,@capital,@region,@subregion,@population)";


                    SqlCommand cmd = new SqlCommand(script, connection);
                    cmd.Parameters.AddWithValue("@nombre", datosPais.nombre);
                    cmd.Parameters.AddWithValue("@capital", datosPais.capital);
                    cmd.Parameters.AddWithValue("@region", datosPais.region);
                    cmd.Parameters.AddWithValue("@subregion", datosPais.subregion);
                    cmd.Parameters.AddWithValue("@population", datosPais.population);


                    cmd.ExecuteReader();

                    
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            //return datosPais;
        }

        public clases.Paisdb obtenerPais(String nombrePais)
        {
            clases.Paisdb pais = new clases.Paisdb();
            using (var connection = new SqlConnection(this.cadenaDeConexionInterna))
            {
                try
                {
                    connection.Open();
                    string script = $"SELECT * FROM pais where pais_nombre='{nombrePais}'";
                    SqlCommand cmd = new SqlCommand(script, connection);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {

                        pais.nombre = reader["pais_nombre"].ToString();
                        pais.capital = reader["pais_capital"].ToString();
                        pais.region = reader["pais_region"].ToString();
                        pais.subregion = reader["pais_subregion"].ToString();
                        pais.population = reader["pais_population"].ToString();
                       

                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return pais;



        }

        public void eliminarPais(string nombrePais)
        {
            using (var connection = new SqlConnection(this.cadenaDeConexionInterna))
            {
                try
                {
                    connection.Open();
                    string script = $"delete from pais where pais_nombre='{nombrePais}'";

                    SqlCommand cmd = new SqlCommand(script, connection);
                    cmd.ExecuteReader();
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public List<clases.Paisdb> ObtenerTodosPaises() {
            List<clases.Paisdb> listaPaises = new List<clases.Paisdb>();
            
            using (var connection = new SqlConnection(this.cadenaDeConexionInterna))
            {
                try
                {
                    connection.Open();
                    string script = $"SELECT * FROM pais";
                    SqlCommand cmd = new SqlCommand(script, connection);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        clases.Paisdb pais = new clases.Paisdb();
                        pais.nombre = reader["pais_nombre"].ToString();
                        pais.capital = reader["pais_capital"].ToString();
                        pais.region = reader["pais_region"].ToString();
                        pais.subregion = reader["pais_subregion"].ToString();
                        pais.population = reader["pais_population"].ToString();

                        listaPaises.Add(pais);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return listaPaises;
        }

        public clases.Paisdb actualizarPais(clases.Paisdb objetoActualizar)
        {

            using (var connection = new SqlConnection(this.cadenaDeConexionInterna))
            {
                try
                {
                    connection.Open();
                    string script = "UPDATE pais SET pais_population=@population, pais_capital=@capital WHERE pais_nombre =@nombre";

                    SqlCommand cmd = new SqlCommand(script, connection);
                    cmd.Parameters.AddWithValue("@population", objetoActualizar.population);
                    cmd.Parameters.AddWithValue("@capital", objetoActualizar.capital);
                    cmd.Parameters.AddWithValue("@nombre", objetoActualizar.nombre);


                    SqlDataReader reader = cmd.ExecuteReader();


                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            var objetoActualizado = obtenerPais(objetoActualizar.nombre);
            return objetoActualizado;

        }

    }

}
