﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace accesoDatos.repositorio.core
{
    public class MSSQLAdministradorConexion
    {
        protected string cadenaDeConexionInterna { get; set; }
        public MSSQLAdministradorConexion(string cadenaDeConexion)
        {
            cadenaDeConexionInterna = cadenaDeConexion;
        }
    }
}
