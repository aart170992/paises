﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace negocio.negociosPais
{
    public class AdministradorDePais
    {
        protected string cadenaDeConexion = "";
        public AdministradorDePais(string cadenaConexion) {
            cadenaDeConexion = cadenaConexion;
        }
        public void insertarPais(List<NegocioPais> datosPais)
        {
            
            accesoDatos.repositorio.MSSQLRepositorioPais metodosPais = new accesoDatos.repositorio.MSSQLRepositorioPais(cadenaDeConexion);

            foreach (var item in datosPais)
            {
                accesoDatos.clases.Paisdb listaPais = new accesoDatos.clases.Paisdb();

                listaPais.nombre = item.name;
                listaPais.capital = item.capital;
                listaPais.population = item.population;
                listaPais.region = item.region;
                listaPais.subregion = item.subregion;
                  
                
                metodosPais.insertar(listaPais);
            }

            //return null;


            
        }

        public NegocioPais obtenerPais(string nombrePais) {
            accesoDatos.repositorio.MSSQLRepositorioPais metodosPais = new accesoDatos.repositorio.MSSQLRepositorioPais(cadenaDeConexion);
            var paisResult = metodosPais.obtenerPais(nombrePais);
            NegocioPais paisNegocio = new NegocioPais
            {
                name = paisResult.nombre,
                capital = paisResult.capital,
                population = paisResult.population,
                region= paisResult.region,
                subregion=paisResult.subregion
                
            };
            return paisNegocio;
        }

        public void eliminarPais(string nombrePais) {
            accesoDatos.repositorio.MSSQLRepositorioPais metodosPais = new accesoDatos.repositorio.MSSQLRepositorioPais(cadenaDeConexion);
            metodosPais.eliminarPais(nombrePais);

        }

        public List<NegocioPais> obtenerTodosPaises() {
            accesoDatos.repositorio.MSSQLRepositorioPais metodosPais = new accesoDatos.repositorio.MSSQLRepositorioPais(cadenaDeConexion);
            List<NegocioPais> listaPaisesNegocio = new List<NegocioPais>();
            var todosPaises = metodosPais.ObtenerTodosPaises();
            foreach (var item in todosPaises)
            {
                NegocioPais negocioPais = new NegocioPais
                {
                    name = item.nombre,
                    capital=item.capital,
                    population=item.population,
                    region=item.region,
                    subregion=item.subregion
                };
                listaPaisesNegocio.Add(negocioPais);
            }
            return listaPaisesNegocio;
        }

        public NegocioPais actualizarPais(NegocioPais pais) {
            accesoDatos.repositorio.MSSQLRepositorioPais metodosPais = new accesoDatos.repositorio.MSSQLRepositorioPais(cadenaDeConexion);
            accesoDatos.clases.Paisdb paisAccesoDatos = new accesoDatos.clases.Paisdb {
                nombre = pais.name,
                capital = pais.capital,
                population=pais.population
                
            };
            var paisActualizado = metodosPais.actualizarPais(paisAccesoDatos);
            NegocioPais paisNegocioActualizado = new NegocioPais {
                capital=paisActualizado.capital,
                name=paisActualizado.nombre,
                population=paisActualizado.population,
                region=paisActualizado.region,
                subregion=paisActualizado.subregion
            };

            return paisNegocioActualizado;
        }
    }
}
